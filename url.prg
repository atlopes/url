*
* URL
*
* A VFP class to work with URLs
*

* install itself
IF !SYS(16) $ SET("Procedure")
	SET PROCEDURE TO (SYS(16)) ADDITIVE
ENDIF

#INCLUDE "url.h"

DEFINE CLASS URL AS Custom

	ADD OBJECT PROTECTED Components AS Collection

	_memberdata = "<VFPData>" + ;
						'<memberdata name="components" type="method" display="Components"/>' + ;
						'<memberdata name="encode" type="method" display="Encode"/>' + ;
						'<memberdata name="getcomponent" type="method" display="GetComponent"/>' + ;
						'<memberdata name="getquery" type="method" display="GetQuery"/>' + ;
						'<memberdata name="load" type="method" display="Load"/>' + ;
						'<memberdata name="open" type="method" display="Open"/>' + ;
						'<memberdata name="parse" type="method" display="Parse"/>' + ;
						'<memberdata name="setquery" type="method" display="SetQuery"/>' + ;
						'<memberdata name="unencode" type="method" display="Unencode"/>' + ;
						"</VFPData>"

	FUNCTION Destroy
		This.Components.Remove(-1)
	ENDFUNC

	*
	* Parse
	* Breaks a URL into components
	* Scheme://User:Password@Host:Port/Path?Query#Fragment
	* Returns Error/Success
	* Components may be read by method GetComponent()
	FUNCTION Parse (URL AS String) AS Boolean

		ASSERT TYPE("m.URL") == "C" ;
			MESSAGE "String parameter expected."

		LOCAL ToParse AS String
		LOCAL Component AS String
		LOCAL ARRAY Parts(1)
		LOCAL CharIndex AS Integer

		* restart the components collection
		This.Components.Remove(-1)

		m.ToParse = m.URL
		IF LEFT(m.ToParse, 2) == "//"
			m.ToParse = ":" + m.ToParse
		ENDIF

		* get the scheme, if present
		m.CharIndex = AT("://", m.ToParse)
		IF m.CharIndex > 0
			m.Component = LEFT(m.ToParse, m.CharIndex - 1)
			IF !EMPTY(m.Component)
				IF !CHRTRAN(LOWER(m.Component), "abcdefghijklmnopqrstuvwxyz", "") == ""
					RETURN .F.
				ENDIF
				This.Components.Add(m.Component, URL_SCHEME)
			ENDIF
			m.ToParse = SUBSTR(m.ToParse, m.CharIndex + 3)
		ENDIF

		* get the fragment, if present
		m.CharIndex = AT("#", m.ToParse)
		IF m.CharIndex > 0
			m.Component = SUBSTR(m.ToParse, m.CharIndex + 1)
			This.Components.Add(m.Component, URL_FRAGMENT)
			m.ToParse = LEFT(m.ToParse, m.CharIndex - 1)
		ENDIF

		* get the query string, if present
		m.CharIndex = AT("?", m.ToParse)
		IF m.CharIndex > 0
			m.Component = SUBSTR(m.ToParse, m.CharIndex + 1)
			This.Components.Add(m.Component, URL_QUERY)
			m.ToParse = LEFT(m.ToParse, m.CharIndex - 1)
		ENDIF

		* is there a path info?
		m.CharIndex = AT("/", m.ToParse)
		IF m.CharIndex > 0
			This.Components.Add(SUBSTR(m.ToParse, m.CharIndex + 1), URL_PATH)
			m.ToParse = LEFT(m.ToParse, m.CharIndex - 1)
		ENDIF

		* what is left to parse may hold user information
		m.CharIndex = AT("@", m.ToParse)
		IF m.CharIndex > 0
			m.Component = LEFT(m.ToParse, m.CharIndex - 1)
			ALINES(m.Parts, LEFT(m.Component, m.CharIndex - 1), 0, ":")
			This.Components.Add(m.Parts(1), URL_USER)
			* with an optional password?
			IF ALEN(m.Parts) = 2
				This.Components.Add(m.Parts(2), URL_PASSWORD)
			ENDIF
			m.ToParse = SUBSTR(m.ToParse, m.CharIndex + 1)
		ENDIF

		* what is left is now the host 
		ALINES(m.Parts, m.ToParse, 0, ":")
		This.Components.Add(m.Parts(1), URL_HOST)
		* with an optional port?
		IF ALEN(m.Parts) = 2
			This.Components.Add(m.Parts(2), URL_PORT)
		ENDIF
		
		RETURN This.Components.Count != 0

	ENDFUNC

	* GetComponent
	* Fetches the value of a component (or .NULL., if not present) of the parsed URL.
	* Component identifiers are in URL.h
	FUNCTION GetComponent (Component AS String) AS String

		ASSERT TYPE("m.URL") == "C" ;
			MESSAGE "String parameter expected."

		IF This.Components.GetKey(m.Component) != 0
			RETURN This.Components.Item(m.Component)
		ELSE
			RETURN .NULL.
		ENDIF

	ENDFUNC

	* GetQuery
	* Fetches all pairs name=value from a query
	* Returns a Collection
	FUNCTION GetQuery (Query AS String) AS Collection

		ASSERT PCOUNT() = 0 OR TYPE("m.URL") == "C" ;
			MESSAGE "String parameter expected."

		LOCAL PairedQuery AS String
		LOCAL ARRAY Pairs(1), Pair(1)
		LOCAL PairIndex AS Integer
		LOCAL Result AS Collection

		m.Result = CREATEOBJECT("Collection")
		m.PairedQuery = IIF(PCOUNT() = 0, NVL(This.GetComponent(URL_QUERY), ""), m.Query)
		FOR m.PairIndex = 1 TO ALINES(m.Pairs, m.PairedQuery, 0, "&")
			IF ALINES(m.Pair, m.Pairs(m.PairIndex), 0, "=") > 1
				m.Result.Add(m.Pair(2), m.Pair(1))
			ELSE
				m.Result.Add("", m.Pair(1))
			ENDIF
		ENDFOR

		RETURN m.Result

	ENDFUNC

	* SetQuery
	* Sets a name=value based query for getting or posting to a URL
	FUNCTION SetQuery (Pairs AS Collection, UTF8 AS Boolean) AS String

		ASSERT TYPE("m.Pairs") == "O" AND TYPE("m.Pairs.BaseClass") == "C" AND m.Pairs.BaseClass == "Collection" ;
				AND (PCOUNT() = 1 OR TYPE("m.UTF8") == "L") ;
			MESSAGE "Collection and logical parameters expected."

		LOCAL PairIndex AS Integer
		LOCAL Query AS String
		LOCAL KeyName AS String

		m.Query = ""
		FOR m.PairIndex = 1 TO m.Pairs.Count
			IF !EMPTY(m.Query)
				m.Query = m.Query + "&"
			ENDIF
			m.KeyName = m.Pairs.GetKey(m.PairIndex)
			IF !EMPTY(m.KeyName)
				m.Query = m.Query + This.Encode(m.KeyName, m.UTF8) + "="
			ENDIF
			m.Query = m.Query + This.Encode(m.Pairs.Item(m.PairIndex), m.UTF8)
		ENDFOR

		RETURN m.Query
	ENDFUNC

	* Encode
	* URL-encodes a string (optionally, converting it to UTF-8 upfront)
	FUNCTION Encode (Unencoded AS String, UTF8 AS Boolean) AS String

		ASSERT TYPE("m.URL") == "C" AND (PCOUNT() = 1 OR TYPE("m.UTF8") == "L") ;
			MESSAGE "String and logical parameters expected."

		LOCAL OriginalText AS String
		LOCAL Encoded AS String
		LOCAL CharIndex AS Integer
		LOCAL CharAt AS Character
		
		IF m.UTF8
			m.OriginalText = STRCONV(STRCONV(m.Unencoded, 1), 9)
		ELSE
			m.OriginalText = m.Unencoded
		ENDIF

		* the URL encoded string, so far
		m.Encoded = ""
		FOR m.CharIndex = 1 TO LEN(m.OriginalText)

			m.CharAt = SUBSTR(m.OriginalText, m.CharIndex, 1)

			IF m.CharAt == " "
				m.CharAt = "+"
			ELSE
				IF ASC(m.CharAt) >= 128 OR m.CharAt $ '%?+@#/:="&' OR m.CharAt < " "
					m.CharAt = "%" + STRCONV(m.CharAt, 15)
				ENDIF
			ENDIF

			m.Encoded = m.Encoded + m.CharAt

		ENDFOR

		RETURN m.Encoded
	ENDFUNC

	* Unencode
	* Transforms a URL encoded string into ANSI (eventually unencoding it from UTF8, also)
	FUNCTION Unencode (Encoded AS String, UTF8 AS Boolean) AS String

		ASSERT TYPE("m.URL") == "C" AND (PCOUNT() = 1 OR TYPE("m.UTF8") == "L") ;
			MESSAGE "String and logical parameters expected."

		LOCAL Unencoded AS String
		LOCAL CharIndex AS Integer
		LOCAL CharAt AS Character

		* the unencoded string, so far
		m.Unencoded = ""
		m.CharIndex = 1

		DO WHILE m.CharIndex <= LEN(m.Encoded)

			m.CharAt = SUBSTR(m.Encoded, m.CharIndex, 1)

			DO CASE
			* special space case, urlencoded as plus
			CASE m.CharAt == "+"
				m.Unencoded = m.Unencoded + " "

			* unencode hexa escaped characters
			CASE m.CharAt == "%"
				m.Unencoded = m.Unencoded + CHR(VAL("0x" + SUBSTR(m.Encoded, m.CharIndex + 1, 2)))
				m.CharIndex = m.CharIndex + 2

			* character is already fine, just read it
			OTHERWISE
				m.Unencoded = m.Unencoded + m.CharAt
			ENDCASE

			m.CharIndex = m.CharIndex + 1
		ENDDO

		RETURN IIF(m.UTF8, STRCONV(STRCONV(m.Unencoded, 11), 2), m.Unencoded)
			
	ENDFUNC

	* Load
	* Simple fetch of a resource contents pointed by a URL
	* Post data, username and password are optional (also if .NULL. or "")
	* Returns URL contents, or .NULL. on error
	FUNCTION Load (URL AS String, PostData AS String, Username AS String, Password AS String) AS String

		LOCAL HTTP AS MSXML2.ServerXMLHTTP60
		LOCAL Fetched AS String

		TRY
			m.HTTP = CREATEOBJECT("MSXML2.ServerXMLHTTP.6.0")
			m.HTTP.open(IIF(EMPTY(NVL(m.PostData, "")), "Get", "Post"), m.URL, .F., EVL(NVL(m.Username, ""), ""), EVL(NVL(m.Password, ""), ""))
			IF !ISNULL(m.PostData) AND !EMPTY(m.PostData)
				m.HTTP.send(m.PostData)
			ELSE
				m.HTTP.send()
			ENDIF
			m.Fetched = "" + m.HTTP.responseBody
		CATCH
			m.Fetched = .NULL.
		ENDTRY

		m.HTTP = .NULL.

		RETURN m.Fetched
	ENDFUNC

	* Open
	* Opens a URL in the default browser
	PROCEDURE Open (URL AS String)

		ASSERT TYPE("m.URL") == "C" ;
			MESSAGE "String parameter expected."

		LOCAL WShell AS Shell.Application

		m.WShell = CREATEOBJECT("Shell.Application")
		m.WShell.ShellExecute(m.URL, "", "", "open", 1)

		m.WShell = .NULL.
	ENDPROC
ENDDEFINE