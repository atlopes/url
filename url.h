* URL.prg accompanying header file

#DEFINE	URL_SCHEME		"Scheme"
#DEFINE	URL_HOST			"Host"
#DEFINE	URL_PORT			"Port"
#DEFINE	URL_USER			"User"
#DEFINE	URL_PASSWORD	"Password"
#DEFINE	URL_PATH			"Path"
#DEFINE	URL_QUERY		"Query"
#DEFINE	URL_FRAGMENT	"Fragment"
